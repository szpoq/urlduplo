var url = window.location + 'updateField';
var urlSimilarity = window.location + 'updateSimilarity';
var row = "row_";
var editLink = "edit_button_";
var updateButton = "update_button_";
var resetButton = "reset_button_";
var textInput = "text_";

	function hasClass(ele, cls) {
		return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
	}
	function addClass(ele, cls) {
		if (!this.hasClass(ele, cls)) ele.className += " " + cls;

	}
	function removeClass(ele, cls) {
		if (hasClass(ele, cls)) {
			var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
			ele.className = ele.className.replace(reg, ' ');
		}
	}
	function swapStoH(target) {
		var targetElement =  document.getElementById(target);
		removeClass(targetElement, 'show');
		addClass(targetElement, 'hide');
	}
	function swapHtoS(target) {
		var targetElement =  document.getElementById(target);
		removeClass(targetElement, 'hide');
		addClass(targetElement, 'show');
	}
	function disableEdit(product_id, key) {
		swapStoH(textInput + key + '_' + product_id);
		swapHtoS(editLink + key + '_' + product_id);
		swapStoH(updateButton + key + '_' + product_id);
		swapStoH(resetButton + key + '_' + product_id);
	}
	function enableEdit(product_id, key) {
		swapHtoS(textInput + key + '_' + product_id);
		swapStoH(editLink + key + '_' + product_id);
		swapHtoS(updateButton + key + '_' + product_id);
		document.getElementById(updateButton + key + '_' + product_id).disabled = false;
		swapHtoS(resetButton + key + '_' + product_id);
	}
	function updateField(product_id, key, store_id) {
		var updateVal = document.getElementById(textInput + key + '_' + product_id).value;
		document.getElementById(updateButton + key + '_' + product_id).disabled = true;
		new Ajax.Request(url, {
			method: 'post',
			parameters: {id: product_id, key: key, value: updateVal, store_id: store_id},
			onComplete: function () {
				document.getElementById(key + '_span_' + product_id).value = key;
				disableEdit(product_id, key);
				document.getElementById(updateButton + key + '_' + product_id).disabled = false;
				addClass(document.getElementById(row + product_id), 'editSuccess');
				document.getElementById(key + '_span_' + product_id).innerHTML = updateVal;
			},
			onError: function () {
				addClass(document.getElementById(row + product_id), 'editError');
			}
		});
	}
	function updateSimilarity() {
		var similarity = document.getElementById('similarity').value;
		new Ajax.Request(urlSimilarity, {
			method: 'post',
			parameters: {similarity: similarity},
			onComplete: function () {
				window.location.reload();
			},
			onError: function () {
				console.log('error');
			}
		});
	}
	function numeralsOnly(evt) {
		evt = (evt) ? evt : event;
		var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
			((evt.which) ? evt.which : 0));
		if (charCode < 48 || charCode > 57) {
			return false;
		}
		return true;
	}

