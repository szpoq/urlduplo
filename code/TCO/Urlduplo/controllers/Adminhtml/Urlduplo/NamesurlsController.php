<?php

class TCO_Urlduplo_Adminhtml_Urlduplo_NamesurlsController extends Mage_Adminhtml_Controller_Action
{
    protected $checkIfTcoIncidentsExists = false;
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
//          //push incident on refresh, only for test
//        if ( $model = Mage::getModel('tco_urlduplo/incident_duplicateurl')){
//            $model->push();
//        }
    }

    public function updateFieldAction(){
        try {
            $data['product_id'] = $this->getRequest()->getParam('id');
            $data['attribute_name'] = $this->getRequest()->getParam('key');
            $data['value'] = $this->getRequest()->getParam('value');
            $response['message'] = $data;
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            Mage::getModel('catalog/product_action')->updateAttributes(array($data['product_id']), array($data['attribute_name']=>$data['value']), 0);
        }
        catch (Exception $e){
            $response['message'] = $e;
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        }
    }
    public function updateSimilarityAction(){
        try {
            $similarity = $this->getRequest()->getParam('similarity');
            $response['similarity'] = $similarity;
            Mage::getModel('core/config')->saveConfig('tco_urlduplo/general/similarity', $similarity);
            Mage::app()->getCacheInstance()->cleanType('config');
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        }
        catch (Exception $e){
            $response['message'] = $e;
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        }
    }
    
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('tco/tco_urlduplo/duplicateurls');
    }
}
