<?php
class TCO_Urlduplo_Adminhtml_Urlduplo_DuplicateurlsController extends Mage_Adminhtml_Controller_Action
{
    protected $checkIfTcoIncidentsExists = false;
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('tco_urlduplo')->__('Urlduplo'))
            ->_title(Mage::helper('tco_urlduplo')->__('duplicateurls Duplicateurls'));
        $this->renderLayout();

    }

    public function updateFieldAction(){
        try {
            $id = $this->getRequest()->getParam('id');
            $attribute = $this->getRequest()->getParam('key');
            $value = $this->getRequest()->getParam('value');
            $storeId = $this->getRequest()->getParam('store_id');
//            $product = Mage::getModel('catalog/product')->load($id);
//            $product->setData('url_key',$urlKey);
//            $product->save();
            Mage::getModel('catalog/product_action')->updateAttributes(array($id), array($attribute=>$value), $storeId);
            
            $response['message'] = $id.' updated';
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        }
        catch (Exception $e){
            $response['message'] = $e;
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        }
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('tco/tco_urlduplo/duplicateurls');
    }
}
