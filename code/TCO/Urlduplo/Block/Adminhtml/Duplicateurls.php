<?php
class TCO_Urlduplo_Block_Adminhtml_Duplicateurls extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('report/duplicateurls.phtml');
    }
 
    public function getArrayWithReport()
    {
//        $storeId = Mage::app()->getAnyStoreView()->getId();
        $eavConfig = Mage::getSingleton('eav/config');
        $source = Mage::getResourceModel('catalog/product');
        $attr    = $eavConfig->getAttribute('catalog_product', 'url_key');
        /* @var $resource Mage_Core_Model_Resource */
        $resource   = Mage::getSingleton('core/resource');
        /* @var $connection Magento_Db_Adapter_Pdo_Mysql */
        $connection = $resource->getConnection('write');
        $allStores = Mage::app()->getStores();
        $stores['0']= 'default/admin';
        foreach ($allStores as $_eachStoreId => $val)
        {
            $storeName = Mage::app()->getStore($_eachStoreId)->getName();
            $storeId = Mage::app()->getStore($_eachStoreId)->getId();
            $stores[$storeId]=$storeName;
        }
        $select = $connection->select()
            ->from(['a1' => $attr->getBackendTable()],['url_key'=>'value', 'entity_id'])
            ->joinInner(array('a2' => $attr->getBackendTable()),
                'a1.attribute_id = a2.attribute_id AND a2.value = a1.value AND a1.entity_id != a2.entity_id' ,'store_id')
            ->joinInner(array('e1' => $source->getTable('catalog/product')), 'a1.entity_id = e1.entity_id',['type_id'])
            ->where('a1.attribute_id = ?', $attr->getAttributeId())
            ->group('a1.entity_id')
            ->order('a1.value');

        $read = Mage::getSingleton( 'core/resource' )->getConnection( 'core_read' );
        $results = $read->fetchAll($select);

        //just in case
        $output = [];

        foreach ($results as $result){
            $result['store_name'] = $stores[$result['store_id']];
            $output[] = $result;
        }
        return $output;
    }

    public static function getBaseEditUrl()
    {
        return Mage::helper("adminhtml")->getUrl('*/catalog_product/edit');
    }
    
}
