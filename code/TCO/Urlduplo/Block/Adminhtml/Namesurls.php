<?php
class TCO_Urlduplo_Block_Adminhtml_Namesurls extends Mage_Core_Block_Template
{
    public function __construct()
    {

        parent::__construct();
        $this->setTemplate('report/duplicateurls.phtml');
    }

    public function getSimilarity(){
        $similarity = Mage::getStoreConfig('tco_urlduplo/general/similarity');
        if ($similarity > 0){
            return $similarity;
        }
        else{
            return 50;
        }
    }    

    public function getArrayWithReport()
    {
        $eavConfig = Mage::getSingleton('eav/config');
        $attrUrlKey    = $eavConfig->getAttribute('catalog_product', 'url_key');
        $attrName    = $eavConfig->getAttribute('catalog_product', 'name');

        /* @var $resource Mage_Core_Model_Resource */
        $resource   = Mage::getSingleton('core/resource');
        /* @var $connection Magento_Db_Adapter_Pdo_Mysql */
        $connection = $resource->getConnection('read');

        $allStores = Mage::app()->getStores();
        $stores['0']= 'default/admin';
        foreach ($allStores as $_eachStoreId => $val)
        {
            $storeName = Mage::app()->getStore($_eachStoreId)->getName();
            $storeid = Mage::app()->getStore($_eachStoreId)->getId();
            $stores[$storeid]=$storeName;
        }

        $expression1 = 'a1.entity_id = a2.entity_id AND a2.attribute_id = '.$attrUrlKey->getId();
        $expression2 = 'a1.entity_id = a3.entity_id AND a3.attribute_id = '.$attrName->getId();

        $similarityValue = $this->getSimilarity();

        $select = $connection->select()
           ->from(['a1' => $attrName->getBackendTable()],['entity_id'=>'a1.entity_id'])
            ->joinInner(array('a2' => $attrUrlKey->getBackendTable()), $expression1, ['url_key'=>'a2.value'])
            ->joinInner(array('a3' => $attrName->getBackendTable()), $expression2, ['name'=>'a3.value'])
            ->group(['a1.entity_id', 'store_id'])
            ->order('a1.entity_id')
            ->columns(array('store_id'=>'a1.store_id'));
        $read = Mage::getSingleton( 'core/resource' )->getConnection( 'core_read' );
        $results = $read->fetchAll($select);
        $output=[];
        foreach ($results as $result){
            $result['store_name'] = $stores[$result['store_id']];
            $output[] = $result;
        }

        $results=[];
        foreach ($output as $result) {
            $string1 = str_replace(' ','-',$result['url_key']);
            $string2 = str_replace(' ','-',$result['name']);
            if (strpos($result['url_key'],$result['entity_id']))
            $string2 = $string2.' '.$result['entity_id'];

            similar_text($string1, $string2 ,$similarity);
            $result['similarity']=round($similarity).'%';
            if (round($similarity) < $similarityValue)
                $results[]=$result;
        }

        return $results;
    }


    public static function getBaseEditUrl()
    {
        return Mage::helper("adminhtml")->getUrl('*/catalog_product/edit');
    }
    
}
