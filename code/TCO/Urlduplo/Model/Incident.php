<?php

class TCO_Urlduplo_Model_Incident {
    /** @var $_helper TCO_Incidents_Helper_Data */
    protected $_helper = null;
    protected $_module = "TCO Urlduplo";
    protected $_incidents = [
        "duplicate_url" => "Found duplicate URLs",
    ];
    protected $_code = null;
    public $_events = array();

//    protected $_level = TCO_Incidents_Helper_Error::ERROR;

    public function isEnabled(){
        return class_exists('TCO_Incidents_Helper_Data') ? true : false;
    }

    public function __construct(){
        if ($this->isEnabled()) {
            $this->_helper = Mage::helper('tco_incidents');
            $this->_events = $this->_helper->generateEventData();
        }
    }

    public function push(){
        $this->_helper->open($this->_module . ' - ' . $this->_incidents[$this->_code],$this->_events,TCO_Incidents_Helper_Error::ERROR);
        $this->resetData();
    }

    public function setEvents($data = array()){
        foreach($data as $index => $value){
            $this->_events[$index] = $value;
        }
        return $this;
    }

    public function resetData(){
        $this->_events = $this->_helper->generateEventData();
    }
}